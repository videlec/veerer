Veerer
======

veerer is a Python module to deal with veering triangulations of surfaces and
their associated flat structures. The development has moved to github, check
out https://github.com/flatsurf/veerer/
